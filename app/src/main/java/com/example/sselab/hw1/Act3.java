package com.example.sselab.hw1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act3", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3);
    }

    @Override
    protected void onStart() {
        Log.i("Act3", "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("Act3", "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("Act3", "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("Act3", "onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.i("Act3", "onRestart");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        Log.i("Act3", "onDestroy");
        super.onDestroy();
    }

    public void onClick_Act3(View v) {
        Intent intent = new Intent(Act3.this, Act1.class);
        startActivity(intent);
    }
}
