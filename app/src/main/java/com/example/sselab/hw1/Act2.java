package com.example.sselab.hw1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Act2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act2", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act2);
    }

    @Override
    protected void onStart() {
        Log.i("Act2", "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("Act2", "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("Act2", "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("Act2", "onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.i("Act2", "onRestart");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        Log.i("Act2", "onDestroy");
        super.onDestroy();
    }

    public void onClick_Act2(View v) {
        Intent intent = null;
        switch(v.getId()) {
            case R.id.button3:
                intent = new Intent(Act2.this, Act1.class);
                break;
            case R.id.button4:
                intent = new Intent(Act2.this, Act3.class);
                break;
            case R.id.button5:
                intent = new Intent(Act2.this, Act4.class);
                break;
        }
        startActivity(intent);
    }
}
