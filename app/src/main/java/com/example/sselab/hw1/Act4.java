package com.example.sselab.hw1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Act4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act4", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act4);
    }

    @Override
    protected void onStart() {
        Log.i("Act4", "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("Act4", "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("Act4", "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("Act4", "onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.i("Act4", "onRestart");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        Log.i("Act4", "onDestroy");
        super.onDestroy();
    }

    public void onClick_Act4(View v) {
        Intent intent = new Intent(Act4.this, Act3.class);
        startActivity(intent);
    }
}
