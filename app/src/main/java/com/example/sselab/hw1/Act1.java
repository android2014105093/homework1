package com.example.sselab.hw1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Act1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Act1", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1);
    }

    @Override
    protected void onStart() {
        Log.i("Act1", "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("Act1", "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i("Act1", "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("Act1", "onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.i("Act1", "onRestart");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        Log.i("Act1", "onDestroy");
        super.onDestroy();
    }

    public void onClick_Act1(View v) {
        Intent intent = null;
        switch(v.getId()) {
            case R.id.button:
                intent = new Intent(Act1.this, Act2.class);
                break;
            case R.id.button2:
                intent = new Intent(Act1.this, Act3.class);
                break;
        }
        startActivity(intent);
    }
}
